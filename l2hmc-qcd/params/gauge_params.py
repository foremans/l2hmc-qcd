GAUGE_PARAMS = {                    # default parameters for `GaugeModel`
    #  ------------------- LATTICE PARAMS -------------------
    'space_size': 8,
    'time_size': 8,
    'link_type': 'U1',
    'dim': 2,
    'batch_size': 64,
    'rand': True,
    #  ------------------- DYNAMICS PARAMS -------------------
    'num_steps': 5,
    'eps': 0.2,
    'fixed_beta': False,
    'beta_init': 2.0,
    'beta_final': 5.0,
    #  ------------------- TRAINING PARAMS -------------------
    'lr_init': 0.001,
    'lr_decay_steps': 1000,
    'lr_decay_rate': 0.96,
    'warmup_lr': False,
    'train_steps': 5000,
    #  ------------------- NETWORK PARAMS -------------------
    'network_arch': 'generic',
    #  'data_format': 'channels_last',
    'num_hidden1': 50,
    'num_hidden2': 50,
    'use_bn': False,
    'dropout_prob': 0.0,
    'clip_value': 0.0,
    'summaries': True,
    #  'save_samples': False,
    'eps_fixed': False,
    'hmc': False,
    #  ------------------- LOSS PARAMS -------------------
    'use_nnehmc_loss': False,
    'use_gaussian_loss': False,
    'loss_scale': 1,
    'std_weight': 1.0,
    'aux_weight': 1.0,
    'charge_weight': 0.0,
    'metric': 'cos_diff',
    #  ------------------- MISC. PARAMS -------------------
    'trace': False,
    'profiler': False,
    'gpu': False,
    'horovod': False,
    'comet': False,
    'restore': False,
    'theta': False,
    'num_intra_threads': 0,
    'float64': False,
    'using_hvd': False,
    #####################################
    #  'plot': True
    #  'charge_weight_inference': None,
    #  'save_steps': 2500,
    #  'print_steps': 1,
    #  'logging_steps': 10,
    #  'plot_lf': False,
    #  'loop_net_weights': False,
    #  'run_hmc': False,
    #  'inverse_loss': True,
    #  'nnehmc_loss': False,
}
